package com.epam.aircompany.dao;

import com.epam.aircompany.bean.Position;

/**
 * The Interface IPositionDao describes own DAO methods for entity Position.
 *
 * @author Dzmitry Hrushetski
 */
public interface IPositionDao extends IBaseDao<Position> {

}
